using System;
using System.Collections.Generic;
using NUnit.Framework;
using stock_calculation;

namespace StockCalculationTests
{
    public class CalculationServiceTests
    {
        private readonly ICalculationService CalculationService = new CalculationService();

        [Test]
        public void testCalculateLoss01_Empty()
        {
            var loss = CalculationService.CalculateLoss(new List<int>());
            Assert.AreEqual(0, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss01_Empty()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int>(), 0);
            Assert.AreEqual(0, loss);
        }

        [Test]
        public void testCalculateLoss02_SimpleDesc()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {5, 0});
            Assert.AreEqual(-5, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss02_SimpleDesc()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {5, 4, 3}, 4);
            Assert.AreEqual(4, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss03_SimpleDesc_With_Skipping_Value()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {5, 3, 0}, 4);
            Assert.AreEqual(3, loss);
        }

        [Test]
        public void testCalculateLoss03_SimpleAsc()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {5, 5});
            Assert.AreEqual(0, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss04_SimpleAsc()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {5, 5}, 4);
            Assert.AreEqual(5, loss);
        }

        [Test]
        public void testCalculateLoss04_FullDesc()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {5, 4, 3, 2, 1, 0});
            Assert.AreEqual(-5, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss05_FullDesc()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {5, 4, 3, 2, 1, 0}, 3);
            Assert.AreEqual(3, loss);
        }

        [Test]
        public void testCalculateLoss05_FullAsc()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {0, 1, 2, 3, 4, 5});
            Assert.AreEqual(0, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss06_FullAsc()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {0, 1, 2, 3, 4, 5}, 3);
            Assert.AreEqual(5, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss06_Not_reaching_loss()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {0, 1, 2, 3, 2, 1}, 5);
            Assert.AreEqual(1, loss);
        }

        [Test]
        public void testCalculateLoss06_AscFlat()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {1, 2, 2, 4, 4, 4, 5, 5});
            Assert.AreEqual(0, loss);
        }

        [Test]
        public void testCalculateLoss07_DescStart()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {5, 3, 4, 2, 3, 1});
            Assert.AreEqual(-4, loss);
        }

        [Test]
        public void testCalculateLoss08_DescMiddle()
        {
            var loss = CalculationService.CalculateLoss(new List<int> {3, 2, 4, 2, 1, 5});
            Assert.AreEqual(-3, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss07_DescMiddle()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {3, 2, 4, 2, 1, 5}, 4);
            Assert.AreEqual(5, loss);
        }
        
        [Test]
        public void testCalculateWithStopLoss07_DescMiddle2()
        {
            var loss = CalculationService.CalculateStockValueWithStopLoss(new List<int> {3, 2, 4, 2, 1, 3}, 4);
            Assert.AreEqual(4, loss);
        }

        [Test]
        public void testCalculateLoss09_DescRandom()
        {
            var budgets = new List<int>();
            budgets.Add(100);
            var randomGenerator = new Random();
            for (int loop = 0; loop < 100; loop++)
            {
                for (int i = 99; i > 0; i--)
                {
                    budgets.Add(randomGenerator.Next(0, i));
                }

                var loss = CalculationService.CalculateLoss(budgets);
                Assert.AreEqual(-100, loss);
            }
        }
    }
}