using System.Collections.Generic;

namespace stock_calculation
{
    public interface ICalculationService
    {
        int CalculateLoss(List<int> stockValues);

        int CalculateStockValueWithStopLoss(List<int> stockValues, int stopLoss);
    }
}