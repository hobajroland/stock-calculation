# Stock calculation
Demo solution can be found on the “demo_solution” branch.

### Source: https://www.codingame.com

### Technology
  - .NET

## Introduction
Both of the stories can be a separate dojo session because of the length of the stories.
When you create your solution you should run the tests often!

To resolve the first story you will need to create and implement 
a calculateLoss() method on the CalculatorService class.

To resolve the second story you will need to create and implement 
a calculateStockContribution() method on the CalculatorService class.

## Stories 

### 1. Calculate loss of the stock
You have to analyze a chronological series of stock values. Imagine a line diagram where the axis y means 
the stock value and the axis x means the time. In this diagram you have to find the largest loss in the values. 
The loss will be expressed as the difference in value between t0-t1, t1-t2, etc. 
If there is no loss, the loss will be worth 0.
Be carefull, loss not only means constant decrease in chronological values.
 
- Input: Array of chronological stock values. The values are integers. 
- Output: The maximal loss p, expressed negatively if there is a loss, otherwise 0.


### 2. Calculate Stock Value with stoploss
If the stock value is less or equal than the stoploss value, we immediately sell the stock in order to mitigate 
the loss. If the value becomes greater than the stoploss value, we buy again the stock. If the stock price never reaches
the stoploss value, this feature has no effect.
- Input: The full price of the stock. A stoploss value
- Output: The final value of the stock